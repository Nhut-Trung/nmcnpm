﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using RestaurantManagerSystem.DTO;

namespace RestaurantManagerSystem.DAO
{
    public class LoaiThucDonDAO
    {
        public static bool ThemLoaiThucDon(LoaiThucDonDTO ltd)
        {
            bool kq;
            ltd.MaLoai = MaTuTang();
            string sql = string.Format("insert into LoaiThucDon values ({0}, N'{1}', N'{2}')", ltd.MaLoai, ltd.Nhom, ltd.TenLoai);
            kq = SqlDataAccessHelper.ExecuteNonQuery(sql);
            return kq;
        }

        public static bool CapNhapLoaiThucDon(LoaiThucDonDTO ltd)
        {
            bool kq;
            string sql = string.Format("update LoaiThucDon set Nhom = N'{0}', TenLoai = '{1}' where MaLoai = {2}", ltd.Nhom, ltd.TenLoai, ltd.MaLoai);
            kq = SqlDataAccessHelper.ExecuteNonQuery(sql);
            return kq;
        }

        public static bool XoaLoaiThucDon(int maLoaiTD)
        {
            bool kq;
            string sql = string.Format("delete from LoaiThucDon where MaLoai = {0}", maLoaiTD);
            kq = SqlDataAccessHelper.ExecuteNonQuery(sql);
            return kq;
        }

        public static bool KiemTraTenLoaiTD(string tenLoaiTD, string nhom)
        {
            bool kq;
            string sql = string.Format("select * from LoaiThucDon where TenLoai = N'{0}' and Nhom = N'{1}'", tenLoaiTD, nhom);
            DataTable dt = SqlDataAccessHelper.ExecuteQuery(sql);
            if (dt.Rows.Count > 0)
                kq = false;
            else
                kq = true;
            return kq;
        }

        //rút trích dữ liệu: select 
        public static List<LoaiThucDonDTO> LayDSLoaiThucDon()
        {
            List<LoaiThucDonDTO> _ds = new List<LoaiThucDonDTO>();
            string sql = "select * from LoaiThucDon";
            DataTable dt = SqlDataAccessHelper.ExecuteQuery(sql);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                LoaiThucDonDTO loai = new LoaiThucDonDTO();
                loai.MaLoai = int.Parse(dt.Rows[i]["MaLoai"].ToString());                
                loai.TenLoai = dt.Rows[i]["TenLoai"].ToString();
                _ds.Add(loai);
            }
            return _ds;
        }

        public static DataTable LayDSLTD()
        {
            DataTable dt;
            string sql = "select MaLoai as 'Mã Loại', Nhom as 'Nhóm', TenLoai as 'Tên Loại' from LoaiThucDon";
            dt = SqlDataAccessHelper.ExecuteQuery(sql);
            return dt;
        }

        public static int LayMaLoaiTuTenLoai(string tenLoai)
        {
            int maLoai;
            string sql = string.Format("select MaLoai from LoaiThucDon where TenLoai = N'{0}'", tenLoai);
            DataTable dt = SqlDataAccessHelper.ExecuteQuery(sql);
            if (dt.Rows.Count > 0)
                maLoai = int.Parse(dt.Rows[0]["MaLoai"].ToString());
            else
                return 0;
            return maLoai;
        }

        public static int MaTuTang()
        {
            string sql = "select * from LoaiThucDon";
            DataTable dt = SqlDataAccessHelper.ExecuteQuery(sql);
            int maTuTang = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (int.Parse(dt.Rows[i][0].ToString()) != maTuTang)
                {
                    return maTuTang;
                }
                maTuTang++;
            }
            return maTuTang;
        }
    }
}
