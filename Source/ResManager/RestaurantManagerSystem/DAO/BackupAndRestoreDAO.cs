﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace RestaurantManagerSystem.DAO
{
    public class BackupAndRestoreDAO
    {
        public static bool Backup(string dataName, string pathBackup)
        {

            string strBackup = "exec sp_SaoLuuCSDL '" + dataName + "', '" + pathBackup + "'";
            
            bool kq = SqlDataAccessHelper.ExecuteNonQuery(strBackup);
            if (kq == false)
                return true;
            else
                return false;
        }

        public static bool Restore(string dataName, string pathRestore)
        {
            try
            {
                string strRestore = "exec dbo.sp_PhucHoiCSDL '" + dataName + "', '" + pathRestore + "'";

                bool kq_restore;
                kq_restore = SqlDataAccessHelper.ExecuteNonQuery(strRestore);
                
                if (kq_restore == false)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString(), "Restore database");
                    return false;
            }
        }
    }
}
