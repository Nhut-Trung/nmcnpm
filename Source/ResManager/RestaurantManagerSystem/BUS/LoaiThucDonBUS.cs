﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantManagerSystem.DTO;
using RestaurantManagerSystem.DAO;
using System.Data;

namespace RestaurantManagerSystem.BUS
{
    public class LoaiThucDonBUS
    {
        public static bool ThemLoaiThucDon(LoaiThucDonDTO ltd)
        {
            bool kq = LoaiThucDonDAO.ThemLoaiThucDon(ltd);
            return kq;
        }

        public static bool CapNhatLoaiThucDon(LoaiThucDonDTO ltd)
        {
            bool kq = LoaiThucDonDAO.CapNhapLoaiThucDon(ltd);
            return kq;
        }

        public static bool XoaLoaiThucDon(int maLoaiTD)
        {
            bool kq = LoaiThucDonDAO.XoaLoaiThucDon(maLoaiTD);
            return kq;
        }

        public static bool KiemTraTenLoaiTD(string tenLTD, string nhom)
        {
            bool kq = LoaiThucDonDAO.KiemTraTenLoaiTD(tenLTD, nhom);
            return kq;
        }

        //Rút trích dữ liệu: select
        public static List<LoaiThucDonDTO> LayDSLoaiThucDon()
        {
            List<LoaiThucDonDTO> _ds;
            _ds = LoaiThucDonDAO.LayDSLoaiThucDon();
            return _ds;
        }

        public static DataTable LayDSLTD()
        {
            DataTable _ds = LoaiThucDonDAO.LayDSLTD();
            return _ds;
        }

        public static int LayMaLoaiTuTenLoai(string tenLoai)
        {
            int maLoai = LoaiThucDonDAO.LayMaLoaiTuTenLoai(tenLoai);
            return maLoai;
        }
    }
}
