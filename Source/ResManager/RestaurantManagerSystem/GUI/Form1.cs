﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using RestaurantManagerSystem.DAO;
using DevComponents.DotNetBar;
using RestaurantManagerSystem.GUI;
using RestaurantManagerSystem.DTO;
using RestaurantManagerSystem.BUS;
using RestaurantManagerSystem.Reports;

namespace RestaurantManagerSystem
{
    public partial class frmMain : Office2007Form
    {
        public frmMain()
        {
           
            InitializeComponent();
        }
        private NhanVienDTO _nv = new NhanVienDTO();

        public NhanVienDTO Nv
        {
            get { return _nv; }
            set { _nv = value; }
        }
        int flag;


        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có muốn thoát không?", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                frmDangNhap frm = new frmDangNhap();
                frm.Show();
            }
        }

        /*********************************************************************************************/

        #region Load dữ liệu

        private void Form1_Load(object sender, EventArgs e)
        {
            DuaLoaiThucDonLenCombobox();
            DuaDSHoaDonLenDataGridView();
            DuaBanLenCombobox();
            DuaDSNhanVienLenCombobox();
            DuaDSNhanVienLenDataGridView();
            DuaDSBanDaGoiLenCombobox();
            LoadThongTinNguoiDung();
            DuaDSLoaiTDLenDataGridView();
            diNgaySinh.MaxDate = DateTime.Today;
            dtiNgayAD.MaxDate = DateTime.Today;
        }
        
        public void DuaBanLenCombobox()
        {
            List<BanDTO> _dsban = BanBUS.LayDSBan();
            List<int> _dsBanDaDat = HoaDonBUS.LayDSBanChuaThanhToan();
            List<int> _dsTam = new List<int>();
            for (int i = 0; i < _dsban.Count; i++)
            {
                bool flag = false;
                for (int j = 0; j < _dsBanDaDat.Count; j++)
                {
                    if (_dsban[i].MaBan == _dsBanDaDat[j])
                    {
                        flag = true;
                    }
                }
                if (flag == false)
                {
                    _dsTam.Add(int.Parse(_dsban[i].MaBan.ToString()));
                }
            }
            cmbBan.DataSource = _dsTam;
        }

        public void DuaDSBanDaGoiLenCombobox()
        {
            cmbDSBanCanLapHD.Items.Clear();
            cmbDSBanCapNhat.Items.Clear();
            cmbDSBanCanLapHD.Text = "";
            cmbDSBanCapNhat.Text = "";
            List<int> _dsMaBan = HoaDonBUS.LayDSBanChuaThanhToan();
            for (int i = 0; i < _dsMaBan.Count; i++)
            {
                cmbDSBanCanLapHD.Items.Add(_dsMaBan[i].ToString());
                cmbDSBanCapNhat.Items.Add(_dsMaBan[i].ToString());
            }
        }
        private void LoadThongTinNguoiDung()
        {
            lbTenDN.Text = Nv.TenDN.ToString();
            lbHoTen.Text = Nv.HoTen.ToString();
            lbQuyen.Text = Nv.Quyen.ToString();
        }
        public void DuaLoaiThucDonLenCombobox()
        {
            List<LoaiThucDonDTO> _dsltd = LoaiThucDonBUS.LayDSLoaiThucDon();
            cmbLoaiTD.DataSource = _dsltd;
            cmbLoaiTD.DisplayMember = "TenLoai";
            cmbLoaiTD.ValueMember = "MaLoai";

            cbLoaiTD.DataSource = _dsltd;
            cbLoaiTD.DisplayMember = "TenLoai";
            cbLoaiTD.ValueMember = "MaLoai";

            cmbLoaiTDCN.DataSource = _dsltd;
            cmbLoaiTDCN.DisplayMember = "TenLoai";
            cmbLoaiTDCN.ValueMember = "MaLoai";

            cmbLoaiThucDon.DataSource = _dsltd;
            cmbLoaiThucDon.DisplayMember = "TenLoai";
            cmbLoaiThucDon.ValueMember = "MaLoai";
        }
        
        public void DuaDSNhanVienLenCombobox()
        {
            DataTable _dsNV = NhanVienBUS.LayDSNhanVienTiepTan();
            cmbDSNhanVien.DataSource = _dsNV;
            cmbDSNhanVien.DisplayMember = "Họ Tên";
            cmbDSNhanVien.ValueMember = "Mã NV";
        }

        public void DuaDSHoaDonLenDataGridView()
        {
            DataTable _dshd = HoaDonBUS.LayDSHoaDon();
            dtgDSHD.DataSource = _dshd;
        }

        public void DuaDSNhanVienLenDataGridView()
        {
            DataTable _dsnd = NhanVienBUS.LayDSNhanVien();
            dtgDSNV.DataSource = _dsnd;
        }

        public void DuaDSLoaiTDLenDataGridView()
        {
            DataTable _dsltd = LoaiThucDonBUS.LayDSLTD();
            dgvDSLoaiTD.DataSource = _dsltd;
        }

        #endregion

        /*********************************************************************************************/

        #region Gọi món

        private void cmbKhuyenMai_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void cmbLoaiTD_SelectedValueChanged(object sender, EventArgs e)
        {
            List<ThucDonDTO> _dstd = new List<ThucDonDTO>();
            string tenLoai = cmbLoaiTD.Text.ToString();
            int maLoaiTD = LoaiThucDonBUS.LayMaLoaiTuTenLoai(tenLoai);
            _dstd = ThucDonBUS.LayDSThucDonTheoMaLoai(maLoaiTD);
            lbDSTD.DataSource = _dstd;
            lbDSTD.DisplayMember = "TenTD";
            lbDSTD.ValueMember = "MaTD";
            lbDSTD.ClearSelected();
            tbDonGia.Text = "";
            lbGiaTK.Text = "0";
        }

        private void btThemTDVaoList_Click(object sender, EventArgs e)
        {
            if (tbDonGia.Text != "")
            {
                if (string.IsNullOrEmpty(tbSoKhach.Text.ToString().Trim()))
                {
                    MessageBoxEx.Show("Số lượng khách chưa được input");
                    tbSoKhach.Focus();
                    return;
                }else {
                    if(int.Parse(tbSoKhach.Text.ToString().Trim()) <= 0){
                         MessageBoxEx.Show("Số lượng khách chưa được input");
                        tbSoKhach.SelectAll();
                        return;
                    }
                }

                if (string.IsNullOrEmpty(tbSL.Text.ToString().Trim()))
                {
                    MessageBoxEx.Show("Số lượng khách input không hợp lệ");
                    tbSL.Focus();
                    return;
                }
                int maTD = int.Parse(lbDSTD.SelectedValue.ToString());
                string tenTD = ThucDonBUS.LayTenThucDonTuMaThucDon(maTD);
                bool tonTai = false;
                int dong = 0;
                for (int i = 0; i < lvChiTietGoiMon.Items.Count; i++)
                {
                    if (int.Parse(lvChiTietGoiMon.Items[i].SubItems[0].Text) == maTD)
                    {
                        tonTai = true;
                        dong = i;
                    }
                }
                string soLuong = "1";
                if (tbSL.Text != "")
                {
                    soLuong = tbSL.Text;
                }
                if (tonTai == false)
                {
                    string donGia = tbDonGia.Text;
                    ListViewItem item = new ListViewItem();
                    item.Text = maTD.ToString();
                    item.SubItems.Add(tenTD);
                    item.SubItems.Add(donGia);
                    item.SubItems.Add(soLuong);
                    this.lvChiTietGoiMon.Items.Add(item);
                    tbSL.Text = "1";
                }
                else
                {
                    int sl = int.Parse(lvChiTietGoiMon.Items[dong].SubItems[3].Text) + int.Parse(soLuong);
                    lvChiTietGoiMon.Items[dong].SubItems[3].Text = sl.ToString();
                }
                
            }
            else
            {
                MessageBoxEx.Show("Bạn nhập giá không chính xác!");
            }
            btThemTDVaoList.Focus();
        }

        private void tbSL_Click(object sender, EventArgs e)
        {
            tbSL.Text = "";
        }

        private void btXoaTDDuocChon_Click(object sender, EventArgs e)
        {
            try
            {
                if (lvChiTietGoiMon.SelectedItems.Count > 0)
                {
                    DialogResult result = MessageBox.Show("Bạn có muốn xóa thực đơn này không?", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                    if (result == DialogResult.Yes)
                    {
                        lvChiTietGoiMon.FocusedItem.Remove();
                    }
                }
                else
                {
                    MessageBoxEx.Show("Bạn chưa chọn thực đơn muốn xóa!!");
                }
               
            }
            catch
            {
                MessageBoxEx.Show("Vui lòng chọn ĐT cần xóa!");
            }
        }

        private void btXoaDSTD_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có muốn xóa hết danh sách không?", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
            if (result == DialogResult.Yes)
            {
                lvChiTietGoiMon.Items.Clear();
            }
        }

        private void btLuuGoiMon_Click(object sender, EventArgs e)
        {
            if (lvChiTietGoiMon.Items.Count > 0)
            {
                if (tbSoKhach.Text != "")
                {
                    HoaDonDTO hd = new HoaDonDTO();
                    CT_HoaDonDTO cthd = new CT_HoaDonDTO();
                    hd.MsBan = int.Parse(cmbBan.Text);
                    int maHD = HoaDonBUS.LayMaHoaDonCanLap();
                    hd.TongTien = 0;
                    hd.MsNVLap = _nv.MaNV;

                    hd.MsNVTT = _nv.MaNV;
                    try
                    {
                        int soKhach = int.Parse(tbSoKhach.Text);
                        if (soKhach > 0)
                        {
                            hd.SoKhach = soKhach;

                            bool kq = HoaDonBUS.LapHoaDon(hd);
                            if (kq == true)
                            {
                                for (int i = 0; i < lvChiTietGoiMon.Items.Count; i++)
                                {
                                    cthd.SoHD = hd.SoHD;
                                    cthd.MaTD = int.Parse(lvChiTietGoiMon.Items[i].SubItems[0].Text);
                                    cthd.DonGia = double.Parse(lvChiTietGoiMon.Items[i].SubItems[2].Text);
                                    cthd.SoLuong = int.Parse(lvChiTietGoiMon.Items[i].SubItems[3].Text);
                                    CT_HoaDonBUS.ThemChiTietHoaDon(cthd);
                                }
                                MessageBoxEx.Show("Lưu gọi món thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DuaDSBanDaGoiLenCombobox();
                                DuaBanLenCombobox();
                                lvChiTietGoiMon.Items.Clear();
                                tbDonGia.Text = "";
                                tbSoKhach.Text = "";
                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Số khách phải lớn hơn 0!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            tbSoKhach.Text = "";
                            tbSoKhach.Focus();
                        }
                    }
                    catch
                    {
                        MessageBoxEx.Show("Kiểu dữ liệu số khách không đúng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tbSoKhach.Text = "";
                        tbSoKhach.Focus();
                    }
                }
                else
                {
                    MessageBoxEx.Show("Chưa nhập số lượng khách!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbSoKhach.Text = "";
                    tbSoKhach.Focus();
                }
            }
            else
            {
                MessageBoxEx.Show("Chưa chọn thực đơn!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lbDSTD_Click(object sender, EventArgs e)
        {
            if (cmbKhuyenMai.Text == "")
                cmbKhuyenMai.Text = "0";
            if (!string.IsNullOrEmpty(lbDSTD.SelectedValue.ToString()))
            {
                int maTD = int.Parse(lbDSTD.SelectedValue.ToString());
                double gia = GiaBUS.LayGiaTheoMaThucDon(maTD);
                lbGiaTK.Text = gia.ToString();
            }
            tbDonGia.Text = Convert.ToString(double.Parse(lbGiaTK.Text) - (double.Parse(cmbKhuyenMai.Text) / 100) * double.Parse(lbGiaTK.Text));
        }

        private void cmbKhuyenMai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbDonGia.Text != "")
                    tbDonGia.Text = Convert.ToString(double.Parse(lbGiaTK.Text) - (double.Parse(cmbKhuyenMai.Text) / 100) * double.Parse(lbGiaTK.Text));
            }
            catch { }
        }

        private void tbSoKhach_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbSL_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #endregion

        /*********************************************************************************************/

        #region Cập nhật gọi món

        private void btXoaTDDuocChonCN_Click(object sender, EventArgs e)
        {
            try
            {
                if (lvChiTietGoiMonCN.SelectedItems.Count <= 0)
                {
                    MessageBoxEx.Show("Vui lòng chọn record cần xóa!");
                    return;
                }
                DialogResult result = MessageBox.Show("Bạn có muốn xóa thực đơn này không?", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                if (result == DialogResult.Yes)
                {
                    lvChiTietGoiMonCN.FocusedItem.Remove();
                } 
            }
            catch
            {
                MessageBoxEx.Show("Vui lòng chọn ĐT cần xóa!");
            }
        }

        private void btXoaDSTDCN_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Bạn có muốn xóa hết danh sách không?", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
            if (result == DialogResult.Yes)
            {
                lvChiTietGoiMonCN.Items.Clear();
            }
        }

        private void cmbLoaiTDCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ThucDonDTO> _dstd = new List<ThucDonDTO>();
            string tenLoai = cmbLoaiTDCN.Text.ToString();
            int maLoaiTD = LoaiThucDonBUS.LayMaLoaiTuTenLoai(tenLoai);
            _dstd = ThucDonBUS.LayDSThucDonTheoMaLoai(maLoaiTD);
            lbDSTDCN.DataSource = _dstd;
            lbDSTDCN.DisplayMember = "TenTD";
            lbDSTDCN.ValueMember = "MaTD";
            tbDonGiaCN.Text = "";
            lbGiaTKCN.Text = "0";
        }

        private void btThemTDVaoListCN_Click(object sender, EventArgs e)
        {
            if (cmbDSBanCapNhat.Text != "")
            {
                if (string.IsNullOrEmpty(tbSoKhachCN.Text.ToString().Trim()))
                {
                    MessageBoxEx.Show("Số lượng khách chưa được input");
                    tbSoKhachCN.Focus();
                    return;
                }
                else
                {
                    if (int.Parse(tbSoKhachCN.Text.ToString().Trim()) <= 0)
                    {
                        MessageBoxEx.Show("Số lượng khách input không đúng");
                        tbSoKhachCN.SelectAll();
                        return;
                    }
                }

                if (string.IsNullOrEmpty(tbDonGiaCN.Text.ToString().Trim()))
                {
                    MessageBoxEx.Show("Đơn giá chưa được input");
                    tbDonGiaCN.Focus();
                    return;
                }
                else
                {
                    if (int.Parse(tbDonGiaCN.Text.ToString().Trim()) <= 0)
                    {
                        MessageBoxEx.Show("Đơn giá input không đúng");
                        tbDonGiaCN.SelectAll();
                        return;
                    }
                }
                //tbSLCN
                if (string.IsNullOrEmpty(tbSLCN.Text.ToString().Trim()))
                {
                    MessageBoxEx.Show("Số lượng chưa được input");
                    tbSLCN.Focus();
                    return;
                }
                else
                {
                    if (int.Parse(tbSLCN.Text.ToString().Trim()) <= 0)
                    {
                        MessageBoxEx.Show("Số lượng input không đúng");
                        tbSLCN.SelectAll();
                        return;
                    }
                }

                if (tbDonGiaCN.Text != "")
                {
                    int maTD = int.Parse(lbDSTDCN.SelectedValue.ToString());
                    string tenTD = ThucDonBUS.LayTenThucDonTuMaThucDon(maTD);
                    bool tonTai = false;
                    int dong = 0;
                    for (int i = 0; i < lvChiTietGoiMonCN.Items.Count; i++)
                    {
                        if (lvChiTietGoiMonCN.Items[i].SubItems[0].Text == tenTD)
                        {
                            tonTai = true;
                            dong = i;
                        }
                    }
                    string soLuong = "1";
                    if (tbSL.Text != "")
                    {
                        soLuong = tbSL.Text;
                    }
                    if (tonTai == false)
                    {
                        string donGia = tbDonGiaCN.Text;
                        ListViewItem item = new ListViewItem();
                        item.Text = tenTD;
                        item.SubItems.Add(donGia);
                        item.SubItems.Add(soLuong);
                        this.lvChiTietGoiMonCN.Items.Add(item);
                        tbSL.Text = "1";
                    }
                    else
                    {
                        int sl = int.Parse(lvChiTietGoiMonCN.Items[dong].SubItems[2].Text) + int.Parse(soLuong);
                        lvChiTietGoiMonCN.Items[dong].SubItems[2].Text = sl.ToString();
                    }
                }
                else
                {
                    MessageBoxEx.Show("Bạn nhập giá không chính xác!");
                }
            }
            else
            {
                MessageBoxEx.Show("Chưa chọn bàn cần cập nhật!");
            }
            btThemTDVaoListCN.Focus();
        }

        private void cmbDSBanCapNhat_SelectedIndexChanged(object sender, EventArgs e)
        {
            int SoHD = HoaDonBUS.LaySoHDTuMaBan(int.Parse(cmbDSBanCapNhat.SelectedItem.ToString()));
            DataTable _ds = new DataTable();
            _ds = CT_HoaDonBUS.LayDSCTHDTuMaHD(SoHD);
            lvChiTietGoiMonCN.Items.Clear();
            int soKhach = HoaDonBUS.LaySoKhachTuSoHD(SoHD);
            tbSoKhachCN.Text = soKhach.ToString();
            for (int i = 0; i < +_ds.Rows.Count; i++)
            {
                ListViewItem li = new ListViewItem();
                li.Text = _ds.Rows[i]["Tên TĐ"].ToString();
                li.SubItems.Add(_ds.Rows[i]["Đơn Giá"].ToString());
                li.SubItems.Add(_ds.Rows[i]["Số Lượng"].ToString());
                lvChiTietGoiMonCN.Items.Add(li);
            }
        }

        private void btCapNhatGoiMon_Click(object sender, EventArgs e)
        {
            if (lvChiTietGoiMonCN.Items.Count > 0)
            {
                if (tbSoKhachCN.Text != "")
                {
                    HoaDonDTO hd = new HoaDonDTO();
                    CT_HoaDonDTO cthd = new CT_HoaDonDTO();
                    hd.MsBan = int.Parse(cmbDSBanCapNhat.Text);
                    hd.SoKhach = int.Parse(tbSoKhachCN.Text);
                    hd.SoHD = HoaDonBUS.LaySoHDTuMaBan(int.Parse(cmbDSBanCapNhat.Text));
                    HoaDonBUS.CapNhatSoKhach(hd.SoKhach, hd.SoHD);
                    bool kq = CT_HoaDonBUS.XoaCTHDTheoSoHD(hd.SoHD);

                    for (int i = 0; i < lvChiTietGoiMonCN.Items.Count; i++)
                    {
                        cthd.SoHD = hd.SoHD;
                        cthd.MaTD = ThucDonBUS.LayMaThucDonTuTenThucDon(lvChiTietGoiMonCN.Items[i].SubItems[0].Text);
                        cthd.DonGia = double.Parse(lvChiTietGoiMonCN.Items[i].SubItems[1].Text);
                        cthd.SoLuong = int.Parse(lvChiTietGoiMonCN.Items[i].SubItems[2].Text);
                        CT_HoaDonBUS.ThemChiTietHoaDon(cthd);
                    }
                    if (kq == true)
                    {
                        MessageBoxEx.Show("Cập nhật gọi món thành công!");
                        DuaDSBanDaGoiLenCombobox();
                        DuaBanLenCombobox();
                        lvChiTietGoiMonCN.Items.Clear();
                        tbDonGia.Text = "";
                    }
                }
                else
                {
                    MessageBoxEx.Show("Nhập số lượng khách!");
                }
            }
            else
            {
                MessageBoxEx.Show("Chưa chọn thực đơn!");
            }
        }

        private void lbDSTDCN_Click(object sender, EventArgs e)
        {
            int maTD = int.Parse(lbDSTDCN.SelectedValue.ToString());
            double gia = GiaBUS.LayGiaTheoMaThucDon(maTD);
            lbGiaTKCN.Text = gia.ToString();
            tbDonGiaCN.Text = Convert.ToString(double.Parse(lbGiaTKCN.Text) - (double.Parse(cmbKhuyenMaiCN.Text) / 100) * double.Parse(lbGiaTKCN.Text));
        }

        private void cmbKhuyenMaiCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbDonGiaCN.Text = Convert.ToString(double.Parse(lbGiaTKCN.Text) - (double.Parse(cmbKhuyenMaiCN.Text) / 100) * double.Parse(lbGiaTKCN.Text));
        }

        #endregion

        /*********************************************************************************************/

        #region Lập Hóa Đơn

        private void btTinhTien_Click(object sender, EventArgs e)
        {
            if (cmbDSBanCanLapHD.Text == "")
                MessageBox.Show("Chưa chọn bàn cần tính!");
            else
            {
                double tongTien = 0;
                for (int i = 0; i < lvDSCTGMCanLapHD.Items.Count; i++)
                {
                    double donGia = double.Parse(lvDSCTGMCanLapHD.Items[i].SubItems[1].Text);
                    int soLuong = int.Parse(lvDSCTGMCanLapHD.Items[i].SubItems[2].Text);
                    tongTien += donGia * soLuong;
                }
                lbTongTien.Text = tongTien.ToString();
            }
        }

        private void cmbDSBanCanLapHD_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbDSNhanVien.Text = "";
            lvDSCTGMCanLapHD.Items.Clear();
            DataTable _ds = new DataTable();
            int maBan = int.Parse(cmbDSBanCanLapHD.Text);
            int maHD = HoaDonBUS.LaySoHDTuMaBan(maBan);
            _ds = CT_HoaDonBUS.LayDSCTHDTuMaHD(maHD);
            for (int i = 0; i < _ds.Rows.Count; i++)
            {
                ListViewItem item = new ListViewItem();
                item.Text = _ds.Rows[i]["Tên TĐ"].ToString();
                item.SubItems.Add(_ds.Rows[i]["Đơn Giá"].ToString());
                item.SubItems.Add(_ds.Rows[i]["Số Lượng"].ToString());
                lvDSCTGMCanLapHD.Items.Add(item);
            }
        }

        private void btLapHD_Click(object sender, EventArgs e)
        {
            if (lvDSCTGMCanLapHD.Items.Count > 0)
            {
                if (cmbDSNhanVien.Text == "")
                    MessageBoxEx.Show("Chưa chọn nhân viên tiếp tân!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (lbTongTien.Text == "0")
                        MessageBoxEx.Show("Chưa tính tổng tiền!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        HoaDonDTO hd = new HoaDonDTO();
                        hd.MsNVTT = int.Parse(cmbDSNhanVien.SelectedValue.ToString());
                        hd.MsBan = int.Parse(cmbDSBanCanLapHD.Text);
                        hd.SoHD = HoaDonBUS.LaySoHDTuMaBan(hd.MsBan);
                        hd.TongTien = double.Parse(lbTongTien.Text);
                        bool kq = HoaDonBUS.CapNhatLapHoaDon(hd);
                        if (kq == true)
                        {
                            MessageBoxEx.Show("Lập hóa đơn thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DuaDSHoaDonLenDataGridView();
                            DuaBanLenCombobox();
                            lvDSCTGMCanLapHD.Items.Clear();
                            DuaDSBanDaGoiLenCombobox();
                            lbTongTien.Text = "0";
                            DialogResult result = MessageBox.Show("Bạn có muốn in hóa đơn này ra không!", "Thông báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                            if (result == DialogResult.Yes)
                            {

                                ViewHoaDon frm = new ViewHoaDon();
                                frm.SoHD = hd.SoHD;
                                frm.ShowDialog();
                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Lập hóa đơn thất bại!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Chưa chọn bàn!");
            }

        }

        #endregion

        /*********************************************************************************************/

        #region Quản lý Hóa Đơn

        private void dtgDSHD_Click(object sender, EventArgs e)
        {
            try
            {
                int idx = dtgDSHD.CurrentRow.Index;
                int maHD = int.Parse(dtgDSHD.Rows[idx].Cells[0].Value.ToString());
                DataTable _ds = CT_HoaDonBUS.LayDSCTHDTuMaHD(maHD);
                dtgDSCTHD.DataSource = _ds;
            }
            catch { }                   
        }

        private void btInHD_Click(object sender, EventArgs e)
        {
            try
            {
                ViewHoaDon frm = new ViewHoaDon();
                int idx = dtgDSHD.CurrentRow.Index;
                frm.SoHD = int.Parse(dtgDSHD.Rows[idx].Cells[0].Value.ToString());
                frm.ShowDialog();
            }
            catch
            {
                MessageBoxEx.Show("Chưa chọn hóa đơn cần in!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btXoaHD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                DialogResult result = MessageBox.Show("Chắn chắn xóa?!!!", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                if (result == DialogResult.Yes)
                {
                    try
                    {
                        int idx = dtgDSHD.CurrentRow.Index;
                        int SoHD = int.Parse(dtgDSHD.Rows[idx].Cells[0].Value.ToString());
                        CT_HoaDonBUS.XoaCTHDTheoSoHD(SoHD);
                        HoaDonBUS.XoaHDTheoSoHD(SoHD);
                        MessageBoxEx.Show("Xóa Thành Công!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DuaDSHoaDonLenDataGridView();
                        dtgDSCTHD.DataSource = null;
                        try
                        {
                            int idx2 = dtgDSHD.CurrentRow.Index;
                            int maHD = int.Parse(dtgDSHD.Rows[idx2].Cells[0].Value.ToString());
                            DataTable _ds = CT_HoaDonBUS.LayDSCTHDTuMaHD(maHD);
                            dtgDSCTHD.DataSource = _ds;
                        }
                        catch { }
                    }
                    catch
                    {
                        MessageBoxEx.Show("Không có hóa đơn thanh toán nào trong hệ thống!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btTCHDTheoNgay_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime ngay = dtiNgayTCHD.Value;
                DateTime cur_date = DateTime.Today;
                if (ngay > cur_date)
                    MessageBoxEx.Show("Ngày tra cứu không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    DataTable kq = HoaDonBUS.ThongKeHDTheoNgay(ngay);
                    dtgDSHD.DataSource = kq;
                    if (kq.Rows.Count < 1)
                        MessageBoxEx.Show("Không có hóa đơn nào được lập vào ngày trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
                MessageBoxEx.Show("Tra cứu thất bại! Kiểm tra kết nối cơ sở dữ liệu!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbThangTCHD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbNamTCHD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btTCHDTheoThangNam_Click(object sender, EventArgs e)
        {
            if (cmbThangTCHD.Text == "")
                MessageBoxEx.Show("Chưa nhập tháng cần tra cứu!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (tbNamTCHD.Text == "" || int.Parse(tbNamTCHD.Text) > DateTime.Today.Year)
                    MessageBoxEx.Show("Năm tra cứu không đúng! Vui lòng nhập lại", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    try
                    {
                        int thang = int.Parse(cmbThangTCHD.Text);
                        if (thang < 1 || thang > 12)
                            MessageBoxEx.Show("Tháng tra cứu không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            int nam = int.Parse(tbNamTCHD.Text);
                            DataTable dt = HoaDonBUS.ThongKeHDTheoThang(thang, nam);
                            dtgDSHD.DataSource = dt;
                            if (dt.Rows.Count < 1)
                                MessageBoxEx.Show("Không có hóa đơn nào được lập trong thời gian trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch
                    {
                        MessageBoxEx.Show("Tra cứu thất bại! Kiểm tra kết nối cơ sở dữ liệu!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btTCHDTheoKhoangNgay_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime tuNgay = dtiTuNgayTCHD.Value;
                DateTime denNgay = dtiDenNgayTCHD.Value;

                if (tuNgay >= denNgay || tuNgay >= DateTime.Today)
                    MessageBoxEx.Show("Mốc ngày bị sai! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    DataTable dt = HoaDonBUS.ThongKeHDTheoKhoangNgay(tuNgay, denNgay);
                    dtgDSHD.DataSource = dt;
                    if (dt.Rows.Count < 1)
                        MessageBoxEx.Show("Không có hóa đơn nào được lập trong khoảng thời gian trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch
            {
                MessageBoxEx.Show("Chưa chọn mốc ngày tra cứu!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void XoaDSHD(DataTable dt)
        {
            if (_nv.Quyen == "Admin")
            {
                DialogResult result = MessageBox.Show("Chắn chắn xóa tất cả hóa đơn được lập trong thời gian trên?!!!", "Cảnh Báo!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                if (result == DialogResult.Yes)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        int soHD = int.Parse(dt.Rows[i][0].ToString());
                        CT_HoaDonBUS.XoaCTHDTheoSoHD(soHD);
                        HoaDonBUS.XoaHDTheoSoHD(soHD);
                    }

                    MessageBoxEx.Show("Đã xóa hóa đơn!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    DuaDSHoaDonLenDataGridView();
                    dtgDSCTHD.DataSource = null;

                    try
                    {
                        int idx2 = dtgDSHD.CurrentRow.Index;
                        int maHD = int.Parse(dtgDSHD.Rows[idx2].Cells[0].Value.ToString());
                        DataTable _ds = CT_HoaDonBUS.LayDSCTHDTuMaHD(maHD);
                        dtgDSCTHD.DataSource = _ds;
                    }
                    catch { }
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btXoaHDTheoNgay_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime ngay = dtiNgayTCHD.Value;
                DataTable dt = HoaDonBUS.ThongKeHDTheoNgay(ngay);
                XoaDSHD(dt);
            }
            catch
            {
                MessageBoxEx.Show("Xóa hóa đơn không thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btXoaHDTheoThangNam_Click(object sender, EventArgs e)
        {
            try
            {
                int thang = int.Parse(cmbThangTCHD.Text);
                int nam = int.Parse(tbNamTCHD.Text);
                DataTable dt = HoaDonBUS.ThongKeHDTheoThang(thang, nam);
                dtgDSHD.DataSource = dt;
                XoaDSHD(dt);
            }
            catch
            {
                MessageBoxEx.Show("Xóa hóa đơn không thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btXoaHDTheoKhoangNgay_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime tuNgay = dtiTuNgayTCHD.Value;
                DateTime denNgay = dtiDenNgayTCHD.Value;

                DataTable dt = HoaDonBUS.ThongKeHDTheoKhoangNgay(tuNgay, denNgay);
                dtgDSHD.DataSource = dt;
                XoaDSHD(dt);
            }
            catch
            {
                MessageBoxEx.Show("Xóa hóa đơn không thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        /*********************************************************************************************/

        #region Nhân Viên

        private void dtgDSNV_Click(object sender, EventArgs e)
        {
            int idx = dtgDSNV.CurrentRow.Index;
            if (!string.IsNullOrEmpty(dtgDSNV.Rows[idx].Cells[1].Value.ToString()))
            {
                tbHoTen.Text = dtgDSNV.Rows[idx].Cells[1].Value.ToString();
            }
            else
            {
                tbHoTen.Text = "";
            }
            if (!string.IsNullOrEmpty(dtgDSNV.Rows[idx].Cells[2].Value.ToString()))
            {
                diNgaySinh.Text = dtgDSNV.Rows[idx].Cells[2].Value.ToString();
            }
            else
            {
                diNgaySinh.Text = "";
            }
            if (!string.IsNullOrEmpty(dtgDSNV.Rows[idx].Cells[3].Value.ToString()))
            {
                tbTenDN.Text = dtgDSNV.Rows[idx].Cells[3].Value.ToString();
            }
            else
            {
                tbTenDN.Text = "";
            }
            if (!string.IsNullOrEmpty(dtgDSNV.Rows[idx].Cells[4].Value.ToString()))
            {
                cmbQuyen.Text = dtgDSNV.Rows[idx].Cells[4].Value.ToString();
            }
            else
            {
                cmbQuyen.Text = "";
            }
           
            if (cmbQuyen.Text == "Admin")
            {
                tbTenDN.ReadOnly = false;
                tbMatKhau.ReadOnly = false;
                tbMatKhau2.ReadOnly = false;
                cmbQuyen.Enabled = false;
            }
            else
                cmbQuyen.Enabled = true;
            string MatKhau = NhanVienBUS.LayMatKhauTuTenDN(dtgDSNV.Rows[idx].Cells[3].Value.ToString());
            tbMatKhau.Text = MatKhau;
            tbMatKhau2.Text = tbMatKhau.Text;
        }

        public void ThemNhanVien()
        {
            NhanVienDTO nv = new NhanVienDTO();
            nv.HoTen = tbHoTen.Text;
           // nv.NgaySinh = DateTime.ParseExact(diNgaySinh.Text, "dd/MM/yyyy", null);
            nv.TenDN = tbTenDN.Text;
            nv.MatKhau = tbMatKhau.Text;
            nv.Quyen = cmbQuyen.Text;
            if (!NhanVienBUS.KiemTraTenDNTonTai(nv.TenDN, nv.MaNV))
            {
                bool kq = NhanVienBUS.ThemNhanVien(nv, diNgaySinh.Text);
                if (kq == true)
                {
                    MessageBoxEx.Show("Thêm người dùng thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DuaDSNhanVienLenDataGridView();
                    tbTenDN.Text = "";
                    tbMatKhau.Text = "";
                    tbMatKhau2.Text = "";
                    tbHoTen.Text = "";
                    cmbQuyen.Text = "Tiếp Tân";
                }
                else
                {
                    MessageBoxEx.Show("Thêm thất bại!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
                MessageBoxEx.Show("Tên đăng nhập này đã tồn tại!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btThemND_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                if (cmbQuyen.Text != "Tiếp Tân")
                {
                    if (tbTenDN.Text.Length >=6 && tbTenDN.Text.Length <=20)
                    {
                        if (tbHoTen.Text != "")
                        {
                            if (tbMatKhau.Text.Length >=6 && tbMatKhau.Text.Length <=20)
                            {
                                if (tbMatKhau.Text == tbMatKhau2.Text)
                                {
                                    if (diNgaySinh.Text != "")
                                    {
                                        if (diNgaySinh.Value < DateTime.Today)
                                        {
                                            ThemNhanVien();
                                        }
                                        else
                                        {
                                            MessageBoxEx.Show("Ngày sinh nhân viên lỗi! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                    else
                                    {
                                        MessageBoxEx.Show("Ngày sinh không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBoxEx.Show("Mật khẩu không trùng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    tbMatKhau2.Text = "";
                                    tbMatKhau2.Focus();
                                }
                            }
                            else
                            {
                                MessageBoxEx.Show("Mật khẩu phải lớn hơn 5 và nhỏ hơn 21 ký tự!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                tbMatKhau.Text = "";
                                tbMatKhau.Focus();
                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Họ tên không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            tbHoTen.Focus();
                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Tên Đăng nhập phải lớn hơn 5 và nhỏ hơn 21 ký tự!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tbTenDN.Text = "";
                        tbTenDN.Focus();
                    }
                }
                else
                {
                    if (tbHoTen.Text != "")
                    {
                        if (diNgaySinh.Text != "")
                        {
                            if (diNgaySinh.Value < DateTime.Today)
                            {
                                ThemNhanVien();
                            }
                            else
                            {
                                MessageBoxEx.Show("Ngày sinh nhân viên lỗi! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Ngày sinh không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Họ tên nhân viên không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tbHoTen.Focus();
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void SuaNhanVien()
        {
            NhanVienDTO nv = new NhanVienDTO();
            int idx = dtgDSNV.CurrentRow.Index;
            nv.MaNV = int.Parse(dtgDSNV.Rows[idx].Cells[0].Value.ToString());
            nv.HoTen = tbHoTen.Text;
            nv.NgaySinh = DateTime.Parse(diNgaySinh.Text);
            nv.TenDN = tbTenDN.Text;
            nv.MatKhau = tbMatKhau.Text;
            nv.Quyen = cmbQuyen.Text;
            if (diNgaySinh.Value < DateTime.Today)
            {
                if (!NhanVienBUS.KiemTraTenDNTonTai(nv.TenDN, nv.MaNV))
                {
                    bool kq = NhanVienBUS.CapNhatNhanVien(nv);
                    if (kq == true)
                    {
                        MessageBoxEx.Show("Cập nhật thông tin người dùng thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        DuaDSNhanVienLenDataGridView();
                        tbTenDN.Text = "";
                        tbMatKhau.Text = "";
                        tbMatKhau2.Text = "";
                        tbHoTen.Text = "";
                        cmbQuyen.Text = "Tiếp Tân";
                    }
                    else
                    {
                        MessageBoxEx.Show("Cập nhật thất bại!");
                    }
                }
                else
                    MessageBoxEx.Show("Tên đăng nhập này đã tồn tại!");
            }
            else
                MessageBoxEx.Show("Ngày sinh nhân viên lỗi! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btSuaND_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                if (cmbQuyen.Text != "Tiếp Tân")
                {
                    if (tbTenDN.Text.Length >= 6 && tbTenDN.Text.Length <= 20)
                    {
                        if (tbHoTen.Text != "")
                        {
                            if (tbMatKhau.Text.Length >= 6 && tbMatKhau.Text.Length <= 20)
                            {
                                if (tbMatKhau.Text == tbMatKhau2.Text)
                                {
                                    if (diNgaySinh.Text != "")
                                    {
                                        SuaNhanVien();
                                    }
                                    else
                                    {
                                        MessageBoxEx.Show("Ngày sinh không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBoxEx.Show("Mật khẩu không trùng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    tbMatKhau2.Text = "";
                                    tbMatKhau2.Focus();
                                }
                            }
                            else
                            {
                                MessageBoxEx.Show("Mật khẩu phải lớn hơn 5 và nhỏ hơn 21 ký tự!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                tbMatKhau.Text = "";
                                tbMatKhau.Focus();
                            }
                        }
                        else
                        {
                            MessageBoxEx.Show("Họ tên nhân viên không được rỗng!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            tbHoTen.Focus();
                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Tên Đăng nhập phải lớn hơn 5 và nhỏ hơn 21 ký tự!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tbTenDN.Text = "";
                        tbTenDN.Focus();
                    }
                }
                else
                {
                    if (tbHoTen.Text != "")
                    {
                        if (diNgaySinh.Text != "")
                        {
                            SuaNhanVien();
                        }
                        else
                        {
                            MessageBoxEx.Show("Ngày sinh không được rỗng!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBoxEx.Show("Họ tên nhân viên không được rỗng!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btXoaND_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                int idx = dtgDSNV.CurrentRow.Index;
                int maNV = int.Parse(dtgDSNV.Rows[idx].Cells[0].Value.ToString());

                DialogResult result = MessageBox.Show("Chắn chắn xóa?!!!", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                if (result == DialogResult.Yes)
                {
                    string quyen = NhanVienBUS.LayQuyenNVTheoMaNV(maNV);
                    if (quyen != "Admin")
                    {
                        bool kq;
                        try
                        {
                            kq = NhanVienBUS.XoaNhanVien(maNV);
                            if (kq == true)
                            {
                                MessageBoxEx.Show("Đã xóa nhân viên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                DuaDSNhanVienLenDataGridView();
                                tbTenDN.Text = "";
                                tbMatKhau.Text = "";
                                tbMatKhau2.Text = "";
                                tbHoTen.Text = "";
                            }
                            else
                                MessageBoxEx.Show("Xóa nhân viên thất bại!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        catch
                        {
                            MessageBoxEx.Show("Nhân viên đã có trong hóa đơn! Không thể xóa!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                        MessageBoxEx.Show("Không thể xóa tài khoản Admin!", "Thông báo!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmbQuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbQuyen.Text == "Tiếp Tân")
            {
                tbTenDN.Text = "";
                tbMatKhau.Text = "";
                tbMatKhau2.Text = "";
                tbTenDN.ReadOnly = true;
                tbMatKhau.ReadOnly = true;
                tbMatKhau2.ReadOnly = true;
            }
            else
            {
                tbTenDN.ReadOnly = false;
                tbMatKhau.ReadOnly = false;
                tbMatKhau2.ReadOnly = false;
            }
        }

        private void btTraCuuNV_Click(object sender, EventArgs e)
        {
            if (tbTenNVTraCuu.Text == "")
                MessageBoxEx.Show("Chưa nhập tên nhân viên cần tra cứu!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                tbHoTen.Text = "";
                tbTenDN.Text = "";
                tbMatKhau.Text = "";
                tbMatKhau2.Text = "";
                diNgaySinh.Text = "";
                cmbQuyen.Text = "Tiếp Tân";
                DataTable dt = NhanVienBUS.TraCuuNhanVienTheoTen(tbTenNVTraCuu.Text);
                dtgDSNV.DataSource = dt;
                if (dt.Rows.Count < 1)
                    MessageBoxEx.Show("Không có nhân viên nào có tên như trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void tbTenNVTraCuu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btTraCuuNV_Click(sender, e);
        }

        private void btDSNV_Click(object sender, EventArgs e)
        {
            tbHoTen.Text = "";
            tbTenDN.Text = "";
            tbMatKhau.Text = "";
            tbMatKhau2.Text = "";
            diNgaySinh.Text = "";
            cmbQuyen.Text = "Tiếp Tân";
            tbTenNVTraCuu.Text = "";
            DuaDSNhanVienLenDataGridView();
        }

        #endregion

        /*********************************************************************************************/

        #region Loại Thực Đơn

        private void dgvDSLoaiTD_Click(object sender, EventArgs e)
        {
            int idx = dgvDSLoaiTD.CurrentRow.Index;
            tbTenLoaiTD.Text = dgvDSLoaiTD.Rows[idx].Cells[2].Value.ToString();
            cboNhom.Text = dgvDSLoaiTD.Rows[idx].Cells[1].Value.ToString();
        }

        private void btThemLoaiTD_Click(object sender, EventArgs e)
        {
            if (tbTenLoaiTD.Text == "")
                MessageBoxEx.Show("Chưa nhập tên loại thực đơn!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (cboNhom.Text == "")
                    MessageBoxEx.Show("Chưa chọn nhóm loại thực đơn!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    string tenLTD = tbTenLoaiTD.Text;
                    LoaiThucDonDTO ltd = new LoaiThucDonDTO();
                    ltd.TenLoai = tenLTD;
                    ltd.Nhom = cboNhom.Text;
                    bool kt = LoaiThucDonBUS.KiemTraTenLoaiTD(ltd.TenLoai, ltd.Nhom);
                    if (kt == false)
                        MessageBoxEx.Show("Loại thực đơn này đã có! Không thể thêm!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        bool kq = LoaiThucDonBUS.ThemLoaiThucDon(ltd);
                        if (kq == true)
                        {
                            MessageBoxEx.Show("Đã thêm loại thực đơn mới!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            DuaDSLoaiTDLenDataGridView();
                            DuaLoaiThucDonLenCombobox();
                        }
                        else
                            MessageBoxEx.Show("Thêm loại thực đơn thất bại! Kiểm tra kết nối cơ sở dữ liệu!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
            }
        }

        private void btCapNhatLoaiTD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                if (tbTenLoaiTD.Text == "")
                    MessageBoxEx.Show("Chưa nhập tên loại thực đơn!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (cboNhom.Text == "")
                        MessageBoxEx.Show("Chưa chọn nhóm loại thực đơn!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        try
                        {
                            int idx = dgvDSLoaiTD.CurrentRow.Index;
                            string tenLTD = tbTenLoaiTD.Text;
                            LoaiThucDonDTO ltd = new LoaiThucDonDTO();
                            ltd.MaLoai = int.Parse(dgvDSLoaiTD.Rows[idx].Cells[0].Value.ToString());
                            ltd.TenLoai = tenLTD;
                            ltd.Nhom = cboNhom.Text;
                            bool kt = LoaiThucDonBUS.KiemTraTenLoaiTD(ltd.TenLoai, ltd.Nhom);
                            if (kt == false)
                                MessageBoxEx.Show("Loại thực đơn này đã có! Không thể sửa!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            else
                            {
                                bool kq = LoaiThucDonBUS.CapNhatLoaiThucDon(ltd);
                                if (kq == true)
                                {
                                    MessageBoxEx.Show("Đã cập nhật loại thực đơn!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    DuaDSLoaiTDLenDataGridView();
                                    DuaLoaiThucDonLenCombobox();
                                }
                                else
                                    MessageBoxEx.Show("Cập nhật loại thực đơn thất bại! Kiểm tra kết nối cơ sở dữ liệu!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        catch
                        {
                            MessageBoxEx.Show("Chưa chọn loại thực đơn cần cập nhật!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            }
            else
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btXoaLoaiTD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                try
                {
                    int idx = dgvDSLoaiTD.CurrentRow.Index;
                    int maLTD = int.Parse(dgvDSLoaiTD.Rows[idx].Cells[0].Value.ToString());
                    DialogResult result = MessageBox.Show("Chắn chắn xóa?!!!", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                    if (result == DialogResult.Yes)
                    {
                        try
                        {
                            bool kq = LoaiThucDonBUS.XoaLoaiThucDon(maLTD);
                            if (kq == true)
                            {
                                MessageBoxEx.Show("Đã xóa loại thực đơn!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                tbTenLoaiTD.Text = "";
                                cboNhom.Text = "";
                                DuaDSLoaiTDLenDataGridView();
                                DuaLoaiThucDonLenCombobox();
                            }
                        }
                        catch
                        {
                            MessageBoxEx.Show("Loại thực đơn này có chứa các thực đơn. Không thể xóa!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            tbTenLoaiTD.Text = "";
                            cboNhom.Text = "";
                        }
                    }
                }
                catch
                {
                    MessageBoxEx.Show("Chưa chọn loại thực đơn cần xóa!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion

        /*********************************************************************************************/

        #region Thực Đơn

        private void cbLoaiTD_SelectedIndexChanged(object sender, EventArgs e)
        {
            int maLoai = cbLoaiTD.SelectedIndex + 1;
            DataTable dsTD = ThucDonBUS.LayDanhSachTDTheoMaLoai(maLoai);
            dgvDSThucDon.DataSource = dsTD;
            tbTenTDTraCuu.Text = "";
        }

        private void dgvDSThucDon_Click(object sender, EventArgs e)
        {
            int idx = dgvDSThucDon.CurrentRow.Index;
            tbTenTD.Text = dgvDSThucDon.Rows[idx].Cells[1].Value.ToString();
            tbDonGiaTD.Text = dgvDSThucDon.Rows[idx].Cells[2].Value.ToString();
            dtiNgayAD.Text = dgvDSThucDon.Rows[idx].Cells[3].Value.ToString();
            tbDVT.Text = dgvDSThucDon.Rows[idx].Cells[4].Value.ToString();
            cmbLoaiThucDon.Text = dgvDSThucDon.Rows[idx].Cells[5].Value.ToString();
        }

        private void tbDonGiaTD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        public void ThemThucDon()
        {
            ThucDonDTO td = new ThucDonDTO();
            GiaDTO g = new GiaDTO();
            td.MaTD = ThucDonBUS.MaTuTang();
            //td.MaLoai = LoaiThucDonBUS.LayMaLoaiTuTenLoai(cbLoaiTD.Text);
            td.MaLoai = cmbLoaiThucDon.SelectedIndex + 1;
            td.TenTD = tbTenTD.Text;
            td.DonViTinh = tbDVT.Text;

            if (dtiNgayAD.Value <= DateTime.Today)
            {
                g.MaTD = td.MaTD;
                g.NgayADGia = dtiNgayAD.Value;

                try
                {
                    g.Gia = double.Parse(tbDonGiaTD.Text);
                    if (g.Gia > 0)
                    {
                        bool kt = ThucDonBUS.KiemTraTrungTenThucDon(td.TenTD);
                        if (kt == false)
                        {
                            DialogResult result = MessageBox.Show("Tên thực đơn này đã có! Chắc chắn thêm?!!!", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                            if (result == DialogResult.Yes)
                            {
                                bool kq1 = ThucDonBUS.ThemThucDon(td);
                                bool kq2 = GiaBUS.ThemGia(g);
                                if (kq1 == true && kq2 == true)
                                    MessageBoxEx.Show("Đã thêm thực đơn!", "Thông báo!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                cmbLoaiThucDon.Text = "";
                                tbTenTD.Text = "";
                                tbDonGiaTD.Text = "";
                                tbDVT.Text = "";
                            }
                        }
                        else
                        {
                            bool kq1 = ThucDonBUS.ThemThucDon(td);
                            bool kq2 = GiaBUS.ThemGia(g);
                            if (kq1 == true && kq2 == true)
                                MessageBoxEx.Show("Đã thêm thực đơn!", "Thông báo!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                        MessageBoxEx.Show("Đơn giá không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch
                {
                    MessageBoxEx.Show("Thêm thực đơn thất bại! Kiểm tra kết nối cơ sở dữ liệu!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBoxEx.Show("Ngày áp dụng giá không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btThemTD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                if (tbTenTD.Text != "")
                {
                    if (tbDonGiaTD.Text != "")
                    {
                        if (dtiNgayAD.Text != "")
                        {
                            if (tbDVT.Text != "")
                            {
                                ThemThucDon();
                            }
                            else
                                MessageBoxEx.Show("Chưa nhập đơn vị tính!");
                        }
                        else
                            MessageBoxEx.Show("Chưa nhập ngày áp dụng đơn giá!");
                    }
                    else
                        MessageBoxEx.Show("Chưa nhập đơn giá!");
                }
                else
                    MessageBoxEx.Show("Chưa nhập tên thực đơn!");
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btSuaTD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                ThucDonDTO td = new ThucDonDTO();
                GiaDTO g = new GiaDTO();

                int idx = dgvDSThucDon.CurrentRow.Index;
                if (tbTenTD.Text != "")
                {
                    if (tbDVT.Text != "")
                    {
                        td.MaTD = int.Parse(dgvDSThucDon.Rows[idx].Cells[0].Value.ToString());
                        td.MaLoai = LoaiThucDonBUS.LayMaLoaiTuTenLoai(cmbLoaiThucDon.Text);
                        td.TenTD = tbTenTD.Text;
                        td.DonViTinh = tbDVT.Text;
                        g.MaTD = td.MaTD;
                        if (dtiNgayAD.Text != "")
                        {
                            g.NgayADGia = DateTime.Parse(dtiNgayAD.Text);

                            try
                            {
                                double gia = double.Parse(tbDonGiaTD.Text);
                                if (gia > 0)
                                {
                                    g.Gia = gia;
                                    bool kq1 = ThucDonBUS.CapNhatThucDon(td);
                                    bool kq2 = GiaBUS.CapNhatGia(g);
                                    if (kq1 == true && kq2 == true)
                                    {
                                        MessageBoxEx.Show("Cập nhật thực đơn thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        tbTenTD.Text = "";
                                        tbDonGiaTD.Text = "";
                                        dtiNgayAD.Text = "";
                                        tbDVT.Text = "";
                                        cbLoaiTD_SelectedIndexChanged(sender, e);
                                    }
                                    else
                                        MessageBoxEx.Show("Cập nhật thực đơn thất bại!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                else
                                {
                                    MessageBoxEx.Show("Đơn giá phải lớn hơn 0!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    tbDonGiaTD.Text = "";
                                    tbDonGiaTD.Focus();
                                }
                            }
                            catch
                            {
                                MessageBoxEx.Show("Chưa nhập đơn giá!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                tbDonGiaTD.Text = "";
                                tbDonGiaTD.Focus();
                            }
                        }
                        else
                            MessageBoxEx.Show("Chưa nhập ngày áp dụng giá!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBoxEx.Show("Chưa nhập đơn vị tính!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        tbDVT.Text = "";
                        tbDVT.Focus();
                    }
                }
                else
                {
                    MessageBoxEx.Show("Chưa nhập tên thực đơn!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tbTenTD.Text = "";
                    tbTenTD.Focus();
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btXoaTD_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                try
                {
                    int idx = dgvDSThucDon.CurrentRow.Index;
                    int maTD = int.Parse(dgvDSThucDon.Rows[idx].Cells[0].Value.ToString());
                    DateTime ngayAD = DateTime.Parse(dtiNgayAD.Text);
                    DialogResult result = MessageBox.Show("Chắn chắn xóa?!!!", "Cảnh Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign, false);
                    if (result == DialogResult.Yes)
                    {
                        bool kq1, kq2;
                        try
                        {
                            kq1 = GiaBUS.XoaGiaTheoMaTDVaNgayAD(maTD, ngayAD);
                            kq2 = ThucDonBUS.XoaThucDonTheoMaTD(maTD);

                            if (kq1 == true && kq2 == true)
                            {
                                MessageBoxEx.Show("Xóa thành công!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                tbTenTD.Text = "";
                                tbDonGiaTD.Text = "";
                                dtiNgayAD.Text = "";
                                tbDVT.Text = "";

                                if (tbTenTDTraCuu.Text != "")
                                    btTim_Click(sender, e);
                                if (cbLoaiTD.Text != "")
                                    cbLoaiTD_SelectedIndexChanged(sender, e);
                            }
                            else
                                MessageBoxEx.Show("Xóa thất bại!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        catch
                        {
                            MessageBoxEx.Show("Thực đơn đã được gọi món hoặc có trong hóa đơn. Không thể xóa!!!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch
                {
                    MessageBoxEx.Show("Chưa chọn thực đơn cần xóa!", "Lỗi!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btTim_Click(object sender, EventArgs e)
        {
            tbTenTD.Text = "";
            cbLoaiTD.Text = "";
            dtiNgayAD.Text = "";
            tbDonGiaTD.Text = "";
            tbDVT.Text = "";
            if (tbTenTDTraCuu.Text == "")
                MessageBoxEx.Show("Chưa nhập tên thực đơn cần tra cứu!", "Thông báo!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                DataTable kq = ThucDonBUS.TraCuuThucDonTheoTen(tbTenTDTraCuu.Text);
                dgvDSThucDon.DataSource = kq;
            }
        }

        private void tbTenTDTraCuu_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btTim_Click(sender, e);
        }

        #endregion

        /*********************************************************************************************/

        #region Thống Kê

        private void btTKTheoNgay_Click(object sender, EventArgs e)
        {
            if (dtNgayPC.Text != "" && dtNgayPC.Value <= DateTime.Today)
            {
                DateTime ngay = dtNgayPC.Value;
                DataTable kq = HoaDonBUS.ThongKeHDTheoNgay(ngay);
                dgvThongKe.DataSource = kq;
                ThongKe(kq);
                if (kq.Rows.Count > 0)
                    flag = 1;
                else
                {
                    MessageBoxEx.Show("Không có hóa đơn nào được lập vào thời gian trên!", "Thông báo!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    flag = 0;
                }
            }
            else
                MessageBoxEx.Show("Ngày thống kê không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void cbThangTK_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tbNamTK_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btTKTheoThang_Click(object sender, EventArgs e)
        {
            try
            {
                int thang = int.Parse(cbThangTK.Text);
                if (thang >= 1 && thang <= 12)
                {
                    if (tbNamTK.Text != "" && int.Parse(tbNamTK.Text) <= DateTime.Today.Year)
                    {
                        int nam = int.Parse(tbNamTK.Text);
                        DataTable dt = HoaDonBUS.ThongKeHDTheoThang(thang, nam);
                        dgvThongKe.DataSource = dt;
                        ThongKe(dt);
                        if (dt.Rows.Count > 0)
                            flag = 2;
                        else
                        {
                            MessageBoxEx.Show("Không có hóa đơn nào được lập vào thời gian trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            flag = 0;
                        }
                    }
                    else
                        MessageBoxEx.Show("Năm thống kê không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                    MessageBoxEx.Show("Tháng thống kê không đúng! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch
            {
                MessageBoxEx.Show("Chưa nhập tháng cần thống kê!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btTKTheoKhoangNgay_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime tuNgay = dtiTuNgay.Value;
                DateTime denNgay = dtiDenNgay.Value;

                if (tuNgay >= denNgay || tuNgay >= DateTime.Today)
                    MessageBoxEx.Show("Mốc ngày bị sai! Vui lòng nhập lại!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    DataTable dt = HoaDonBUS.ThongKeHDTheoKhoangNgay(tuNgay, denNgay);
                    dgvThongKe.DataSource = dt;
                    ThongKe(dt);
                    if (dt.Rows.Count > 0)
                        flag = 3;
                    else
                    {
                        MessageBoxEx.Show("Không có hóa đơn nào được lập vào thời gian trên!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        flag = 0;
                    }
                }
            }
            catch
            {
                MessageBoxEx.Show("Chưa chọn mốc ngày thống kê!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }            
        }

        public void ThongKe(DataTable kq)
        {
            if (kq.Rows.Count > 0)
            {
                double tongDoanhThu = 0;
                int tongKhachDen = 0;
                for (int i = 0; i < dgvThongKe.Rows.Count - 1; i++)
                {
                    tongDoanhThu += double.Parse(dgvThongKe.Rows[i].Cells[6].Value.ToString());
                    tongKhachDen += int.Parse(dgvThongKe.Rows[i].Cells[3].Value.ToString());
                }
                lbTongDoanhThu.Text = tongDoanhThu.ToString() + " VNÐ";
                lbSoLuongKhachDen.Text = tongKhachDen.ToString() + " Khách";

                DataTable _ds = new DataTable();
                _ds.Columns.Add("SoHD", typeof(int));
                _ds.Columns.Add("MaThucDon", typeof(int));
                _ds.Columns.Add("SoLuong", typeof(int));
                _ds.Columns.Add("DonGia", typeof(double));
                _ds.PrimaryKey = new DataColumn[] { _ds.Columns["SoHD"], _ds.Columns["MaThucDon"] };
                for (int i = 0; i < kq.Rows.Count; i++)
                {
                    int SoHD = int.Parse(kq.Rows[i][0].ToString());
                    if (_ds.Rows.Count == 0)
                    {
                        DataTable dtct = CT_HoaDonBUS.LayDSCTHD(SoHD);
                        for (int j = 0; j < dtct.Rows.Count; j++)
                        {
                            DataRow ct = _ds.NewRow();
                            ct[0] = int.Parse(dtct.Rows[j][0].ToString());
                            ct[1] = int.Parse(dtct.Rows[j][1].ToString());
                            ct[2] = int.Parse(dtct.Rows[j][2].ToString());
                            ct[3] = double.Parse(dtct.Rows[j][3].ToString());
                            _ds.Rows.Add(ct);
                        }
                    }
                    else
                    {
                        DataTable dtct = CT_HoaDonBUS.LayDSCTHD(SoHD);
                        for (int j = 0; j < dtct.Rows.Count; j++)
                        {
                            bool kt = false;
                            int dong = 0;
                            for (int k = 0; k < _ds.Rows.Count; k++)
                            {
                                if (dtct.Rows[j][1].ToString() == _ds.Rows[k][1].ToString())
                                {
                                    dong = k;
                                    kt = true;
                                }
                            }
                            if (kt == true)
                            {
                                _ds.Rows[dong][2] = int.Parse(_ds.Rows[dong][2].ToString()) + int.Parse(dtct.Rows[j][2].ToString());
                            }
                            else
                            {
                                DataRow ct = _ds.NewRow();
                                ct[0] = int.Parse(dtct.Rows[j][0].ToString());
                                ct[1] = int.Parse(dtct.Rows[j][1].ToString());
                                ct[2] = int.Parse(dtct.Rows[j][2].ToString());
                                ct[3] = double.Parse(dtct.Rows[j][3].ToString());
                                _ds.Rows.Add(ct);
                            }
                        }
                    }
                }

                DataTable _dstd = new DataTable();
                _dstd.Columns.Add("SoHD", typeof(int));
                _dstd.Columns.Add("MaThucDon", typeof(int));
                _dstd.Columns.Add("SoLuong", typeof(int));
                _dstd.Columns.Add("DonGia", typeof(double));
                _dstd.PrimaryKey = new DataColumn[] { _dstd.Columns["SoHD"], _dstd.Columns["MaThucDon"] };

                DataTable _dstu = new DataTable();
                _dstu.Columns.Add("SoHD", typeof(int));
                _dstu.Columns.Add("MaThucDon", typeof(int));
                _dstu.Columns.Add("SoLuong", typeof(int));
                _dstu.Columns.Add("DonGia", typeof(double));
                _dstu.PrimaryKey = new DataColumn[] { _dstu.Columns["SoHD"], _dstu.Columns["MaThucDon"] };

                for (int i = 0; i < _ds.Rows.Count; i++)
                {
                    if (ThucDonBUS.KiemTraThucAnNuocUong(int.Parse(_ds.Rows[i][1].ToString())))
                    {
                        DataRow ct = _dstd.NewRow();
                        ct[0] = int.Parse(_ds.Rows[i][0].ToString());
                        ct[1] = int.Parse(_ds.Rows[i][1].ToString());
                        ct[2] = int.Parse(_ds.Rows[i][2].ToString());
                        ct[3] = double.Parse(_ds.Rows[i][3].ToString());
                        _dstd.Rows.Add(ct);
                    }
                    else
                    {
                        DataRow ct = _dstu.NewRow();
                        ct[0] = int.Parse(_ds.Rows[i][0].ToString());
                        ct[1] = int.Parse(_ds.Rows[i][1].ToString());
                        ct[2] = int.Parse(_ds.Rows[i][2].ToString());
                        ct[3] = double.Parse(_ds.Rows[i][3].ToString());
                        _dstu.Rows.Add(ct);
                    }
                }

                if (_dstd.Rows.Count > 0)
                {
                    int MaxTD = int.Parse(_dstd.Rows[0][2].ToString());
                    for (int i = 0; i < _dstd.Rows.Count; i++)
                    {
                        int sl = int.Parse(_dstd.Rows[i][2].ToString());
                        if (MaxTD < sl)
                            MaxTD = int.Parse(_dstd.Rows[i][2].ToString());
                    }
                    int y = 0;
                    for (int i = 0; i < _dstd.Rows.Count; i++)
                    {
                        if (MaxTD == int.Parse(_dstd.Rows[i][2].ToString()))
                            y = i;
                    }

                    int MaTD = int.Parse(_dstd.Rows[y][1].ToString());
                    lbSLTDBanNhieuNhat.Text = MaxTD.ToString();
                    lbTDBanNhieuNhat.Text = ThucDonBUS.LayTenThucDonTuMaThucDon(MaTD);
                    lbDVTDBanNhieuNhat.Text = ThucDonBUS.LayDonViTinhTuMaTD(MaTD);
                }

                if (_dstu.Rows.Count > 0)
                {
                    int MaxTU = 0;
                    for (int i = 0; i < _dstu.Rows.Count; i++)
                    {
                        if (MaxTU < int.Parse(_dstu.Rows[i][2].ToString()))
                            MaxTU = int.Parse(_dstu.Rows[i][2].ToString());
                    }
                    int z = 0;
                    for (int i = 0; i < _dstu.Rows.Count; i++)
                    {
                        if (MaxTU == int.Parse(_dstu.Rows[i][2].ToString()))
                            z = i;
                    }

                    int MaTU = int.Parse(_dstu.Rows[z][1].ToString());
                    lbSLTUBanNhieuNhat.Text = MaxTU.ToString();
                    lbTUBanNhieuNhat.Text = ThucDonBUS.LayTenThucDonTuMaThucDon(MaTU);
                    lbDVTUBanNhieuNhat.Text = ThucDonBUS.LayDonViTinhTuMaTD(MaTU);
                }

            }
            else
            {
                lbTDBanNhieuNhat.Text = "Null";
                lbTUBanNhieuNhat.Text = "Null";
                lbDVTDBanNhieuNhat.Text = "Null";
                lbDVTUBanNhieuNhat.Text = "Null";
                lbSLTDBanNhieuNhat.Text = "0";
                lbSLTUBanNhieuNhat.Text = "0";
            }

        }

        private void btInDSHD_Click(object sender, EventArgs e)
        {
            if (dgvThongKe.Rows.Count < 1 || flag == 0)
            {
                MessageBoxEx.Show("Danh sách rỗng!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (flag == 1)
                {
                    ViewThongKeTheoNgay frm = new ViewThongKeTheoNgay();
                    frm.TongDoanhThu = lbTongDoanhThu.Text;
                    frm.Ngay = dtNgayPC.Value;
                    frm.ShowDialog();
                }
                if (flag == 2)
                {
                    ViewThongKeTheoThangNam frm = new ViewThongKeTheoThangNam();
                    frm.ThangNam = cbThangTK.Text + "/" + tbNamTK.Text;
                    frm.TongDoanhThu = lbTongDoanhThu.Text;
                    frm.ShowDialog();
                }
                if (flag == 3)
                {
                    ViewThongKeTheoKhoangNgay frm = new ViewThongKeTheoKhoangNgay();
                    frm.TongDoanhThu = lbTongDoanhThu.Text;
                    frm.TuNgay = dtiTuNgay.Value;
                    frm.DenNgay = dtiDenNgay.Value;
                    frm.ShowDialog();
                }
            }

        }

        #endregion

        /*********************************************************************************************/

        #region Backup & Restore

        private void btBackupPath_Click(object sender, EventArgs e)
        {
            SaveFileDialog sv = new SaveFileDialog();
            sv.Title = "Chọn đường dẫn sao lưu dữ liệu!";
            sv.Filter = "Tập tinh sao lưu (*.bak)|*.bak";
            sv.ShowDialog();
            txtPathBU.Text = sv.FileName;
        }

        private void btBackup_Click(object sender, EventArgs e)
        {
            if (_nv.Quyen == "Admin")
            {
                if (txtPathBU.Text == "")
                {
                    MessageBoxEx.Show("Chưa nhập đường dẫn lưu tệp tin!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                   
                try
                {
                    string dataName = "ResManager";
                    string pathBackup = txtPathBU.Text;
                    bool kq = BackupAndRestoreBUS.Backup(dataName, pathBackup);
                    if (kq == true)
                    {
                        MessageBoxEx.Show("Dữ liệu đã được sao lưu!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                catch (SqlException ex)
                {
                    MessageBox.Show(ex.ToString(), "Backup Database");
                    return;
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btRestorePath_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Title = "Chọn đường dẫn phục hồi dữ liệu!";
            f.Filter = "Tập tinh phục hồi (*.bak)|*.bak";
            f.ShowDialog();
            txtPathRS.Text = f.FileName;
        }

        private void btRestore_Click(object sender, EventArgs e)
        {
            if(_nv.Quyen=="Admin")
            {
                if (txtPathRS.Text == "")
                    MessageBoxEx.Show("Chưa nhập đường dẫn lưu tệp tin!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                try
                {
                    string dataName = "ResManager";
                    string pathRestore = txtPathRS.Text;
                    bool kq = BackupAndRestoreBUS.Restore(dataName, pathRestore);
                    if (kq == true)
                    {
                        MessageBoxEx.Show("Dữ liệu đã được phục hồi!", "Thông báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Form1_Load(sender, e);
                    }
                }

                catch (SqlException ex)
                {
                    MessageBox.Show(ex.ToString(), "Backup Database");
                    return;
                }
            }
            else
            {
                MessageBoxEx.Show("Chỉ có Admin mới có thể sử dụng chức năng này!", "Thông Báo!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion

        public static int KiemTraKhoangTrang(string text)
        {
            Char[] array = text.ToCharArray();
                        
            //Kiểm tra ký tự đầu là khoảng trắng thì báo lỗi
            if (array[0] == ' ')
            {
                return 1;
            }

            //Kiểm tra 2 khoảng trắng liền nhau trở lên
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == ' ' && array[i + 1] == ' ')
                {
                    return 2;
                }
            }

            //Kiểm tra ký tự cuối cùng là khoảng trắng
            if (array[array.Length - 1] == ' ')
            {
                return 3;
            }
            return 0;
        }

        public static bool KiemTraTenDangNhapVaMatKhau(string account)
        {
            Char[] array = account.ToCharArray();

            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == ' ')
                    return false;
            }
            return true;
        }

        private void dtgDSHD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
           
        }

        private void tiNhanVien_Click(object sender, EventArgs e)
        {
            groupPanel7.Focus();
            dtgDSNV.ClearSelection();
        }

        private void tabItem4_Click(object sender, EventArgs e)
        {
            groupPanel13.Focus();
            dgvDSLoaiTD.ClearSelection();
        }

        private void tiThucDon_Click(object sender, EventArgs e)
        {
            tabControlPanel8.Focus();
            dgvDSThucDon.ClearSelection();
        }

    }
}