using System;
using System.Collections.Generic;
using System.Text;
using RestaurantManagerSystem.DAO;

namespace RestaurantManagerSystem.BUS
{
    public class BackupAndRestoreBUS
    {
        public static bool Backup(string dataName, string pathBackup)
        {
            bool kq = BackupAndRestoreDAO.Backup(dataName, pathBackup);
            return kq;
        }

        public static bool Restore(string dataName, string pathRestore)
        {
            bool kq = BackupAndRestoreDAO.Restore(dataName,pathRestore);
            return kq;
        }
    }
}
